/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.swingapp;

import com.mycompany.swingapp.jdbc.DBConnectionManager;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author RENT
 */
public class JdbcTutorial {

    private static String url = "jdbc:mysql://localhost/books_db?serverTimezone=CET";
    private static String userName = "root";
    private static String password = "";

    public static void main(String[] args) throws ClassNotFoundException, SQLException {

        DBConnectionManager dbConnectionManager = new DBConnectionManager(url, userName, password);
        Connection connection = dbConnectionManager.getConnection();
        java.sql.PreparedStatement ps = null;

        try {
            ps = connection.prepareStatement("select id, first_name, last_name from users");
            ResultSet resultSet = ps.executeQuery();
            
            while(resultSet.next()){ //działa jak kursor - zwraca tru/false jeśli ma gdzie przeskoczyc

            String firstName = resultSet.getString("first_name");
            String lastName = resultSet.getString("last_name");

            System.out.println("First name " + firstName + " last name " + lastName);
            }

        } catch (SQLException e) {
            e.printStackTrace();

        }
    }

}
